package ee.Henn;

import java.util.*;

public class Person {

    private String personalId = "00000000000";
    private String name = "";

    private static int inimesteArv = 0;
    public static List<Person> listOfPeople = new ArrayList<>();

    // täna lisasime need kaks mäppi (sõnastikku)
    public static Map<String, Person> mapOfPeople = new HashMap<>();
    public static Map<String, List<Person>> mapOfPeopleNames = new TreeMap<>();
    // selle esimese, milles on inimesed isikukoodi järgi
    // selle teisem kus on inimesed (mitu - seepärast list) nime järgi


    public static int getInimesteArv() {
        return inimesteArv;
    }


    /**
     * @Author Henn
     * @Date 16.1.2019
     * @
     * * @return private field from instance
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {

        // vanast listist maha

        if(this.name.length() > 0)
        mapOfPeopleNames.get(this.name).remove(this);

        if (name.length() < 1) this.name = "unknown";
        else {

            String[] osad = name.split(" ");
            for (int i = 0; i < osad.length; i++) {
                osad[i] = osad[i].substring(0, 1).toUpperCase() + osad[i].substring(1).toLowerCase();
            }
            this.name = String.join(" ", osad);
        }

        // lisame uude kohta
        if (!mapOfPeopleNames.keySet().contains(name)) {
            mapOfPeopleNames.put(name, new ArrayList<>());
        }
        mapOfPeopleNames.get(name).add(this);


    }

    // see siin on konstruktor
    public Person(String personalId, String name) throws Exception {
        inimesteArv++;
        listOfPeople.add(this); // siin lisasime inimese listi
        // huvitav, kas seda ongi vaja tulevikus,. kui meil juba dictionarid on

        setName(name);
        this.personalId = personalId;

        // iga inimene, kes konstrueeritakse, pannakse isikukoodi järgi dictionay
        if (!mapOfPeople.keySet().contains(personalId)) {
            mapOfPeople.put(personalId, this);
        } else {
            throw new Exception("sellise isikukoodiga inimene on juba ees: " + personalId);
        }
        // NB! kui sama isikukoodiga tuleb uus inimene, siis ei pane
        // me peaks mõtlema, mis teha, et sellest ka teada anda


//        if (!mapOfPeopleNames.keySet().contains(name)) {
//            mapOfPeopleNames.put(name, new ArrayList<>());
//        }
//        mapOfPeopleNames.get(name).add(this);


    }

    public static Person getById(String personalId) {
        return mapOfPeople.keySet().contains(personalId)
                ? mapOfPeople.get(personalId) : null;
    }

    public static Person getById2(String personalId) {
        for (var p : listOfPeople) if (p.personalId.equals(personalId)) return p;
        return null;
    }

    public static List<Person> getByName(String name) {
        return mapOfPeopleNames.keySet().contains(name)
                ? mapOfPeopleNames.get(name) : null;
    }


    public static List<Person> getByName2(String name) {
        List<Person> vastus = new ArrayList<>();
        for (var p : listOfPeople) if (p.name.equalsIgnoreCase(name)) vastus.add(p);
        return vastus;
    }

    public String getPersonalId() {
        return personalId;
    }

    private void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    // selline on vaikimisi (default) konstruktor
    // kui on omatehtud, siis sellist süsteem ise ei tee
    // kui mul on mitu konstruktorit - overloadimine
    Person() throws Exception {
        this("00000000000", "");
    }


    public Person(String personalId) throws Exception {
        this(personalId, "");
    }

    // siin lõppesid konstruktorid ära

    // siit algavad meetodid

    // getterid ja setterid


    public int getBirthYear() {
        return (((Integer.parseInt(personalId.substring(0, 1)) - 1) / 2) * 100
                + 1800 + Integer.parseInt(personalId.substring(1, 3)));
    }

    public Date getBirthDate() {
        return (new GregorianCalendar(
                getBirthYear(),
                Integer.parseInt(personalId.substring(3, 5)) - 1,
                Integer.parseInt(personalId.substring(5, 7))
        )).getTime();
    }

    public String getBirthMonth() {
        // vaja leida isikukoodi järgi sünnikuu 'Märts'
        return String.format("%tB", getBirthDate());


    }


    // siit algavad overridetud meetodid - mis iganes need on
    public String toString() {
        return String.format("%s %s syndinud %td.%<tB %<tY.a.",
                getGender(), name, getBirthDate());
    }

    public Gender getGender() {
        if (Integer.parseInt(personalId.substring(0, 1)) % 2 == 0)
            return Gender.NAINE;
        else
            return Gender.MEES;
    }

}
