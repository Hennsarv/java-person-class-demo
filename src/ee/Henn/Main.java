package ee.Henn;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {




    public static void main(String[] args) throws FileNotFoundException {
        // write your code here
        //    Person person = new Person("35503070211", "henn");
        File peopleData = new File("inimesed.txt");
        //List<Person> people = new ArrayList<>();
        Scanner scan = new Scanner(peopleData);
        while (scan.hasNext()) {
            String rida = scan.nextLine();
            //System.out.println(rida);
            String[] data = rida.split(",");
            try {
                new Person(data[0], data[1]);
            } catch (Exception e) {
                System.out.printf("%s jääb lisamata - topelt isikukood%n", rida);
                System.out.println(e.getMessage());
            }

        }

        System.out.printf("Meil on %s inimest %n", Person.getInimesteArv());

        for(var p : Person.listOfPeople) System.out.println(p);

        // see oleks üks (1) variant, kuidas leida noorim
        Person noorim = Person.listOfPeople.get(0);
        for (int i = 1; i < Person.getInimesteArv(); i++) {
            if (Person.listOfPeople.get(i).getBirthDate().getTime() > noorim.getBirthDate().getTime())
                noorim = Person.listOfPeople.get(i);
        }
        System.out.printf("noorim on %s%n", noorim.getName());

        List<Person> poisid = new ArrayList<>();
        List<Person> tüdrukud = new ArrayList<>();


        for (var p : Person.listOfPeople) {
            if(p.getGender() == Gender.MEES) poisid.add(p);
            else tüdrukud.add(p);
        }

        System.out.println(Person.getById("35503070211").getName());

        System.out.println("otsime kõik ärni õunapuud üles");

        for (var ä : Person.getByName("Ärni Õunapuu")) System.out.println(ä.getPersonalId());

        Person.getById("38001020000").setName("Ärni Õunapuu");

        System.out.println("otsime kõik ärni õunapuud uuesti üles");

        for (var ä : Person.getByName("Ärni Õunapuu")) System.out.println(ä.getPersonalId());




    }
}
